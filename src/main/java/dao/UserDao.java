package dao;

import model.User;
import utils.ConnectionPool;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    public List<User> getAllUsers() {
        Connection connection = ConnectionPool.getInstance().getConnection();
        List<User> users = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM user");
            while (resultSet.next()) {
                users.add(new User(resultSet.getString("name"), resultSet.getString("password"), Integer.parseInt(resultSet.getString("role"))));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public User getUser(String username, String password) {
        Connection connection = ConnectionPool.getInstance().getConnection();
        User user = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet res = statement.executeQuery("SELECT * FROM user WHERE name = \'" + username + "\'");
            while (res.next()) {
                String name = res.getString("name");
                String pass = res.getString("password");
                String role = res.getString("role");
                if (password.equals(pass))
                    user = new User(name, password, Integer.parseInt(role));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }
}
