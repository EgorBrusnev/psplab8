package dao;

import model.Hotel;
import utils.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HotelDao {
    public List<Hotel> getAllHotels() {
        Connection connection = ConnectionPool.getInstance().getConnection();
        List<Hotel> hotels = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM hotel");
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                int number = Integer.parseInt(resultSet.getString("number"));
                String date = resultSet.getString("date");
                hotels.add(new Hotel(name, number, date));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hotels;
    }

    public boolean insertHotel(Hotel hotel) {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT into test.hotel (name, number, date) VALUES (?,?,?)");
            statement.setString(1, hotel.getName());
            statement.setInt(2, hotel.getNumber());
            statement.setString(3, hotel.getDate());
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
