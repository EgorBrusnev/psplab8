package utils;

import dao.UserDao;
import model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

public class AppUtil {
    public static User loggedInUser(HttpServletRequest req) {
        UserDao userDao = new UserDao();
        User user = null;
        HttpSession session = req.getSession();
        if (!session.isNew()) {
            String username = (String) session.getAttribute("name");
            String password = (String) session.getAttribute("password");
            user = userDao.getUser(username, password);
        }
        return user;
    }

    public static void setLocale(HttpServletRequest req) {
        HttpSession session = req.getSession();
        String locale = (String) session.getAttribute("locale");
        Config.set(session, Config.FMT_LOCALE, locale != null ? locale : Locale.ENGLISH.getLanguage());
    }
}
