package servlet;

import dao.UserDao;
import model.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

@WebServlet("/")
public class LoginServlet extends HttpServlet {
    private Logger log = LogManager.getLogger(this.getClass());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getServletPath();
        log.info("doGet: " + pathInfo);
        if ("/logout".equals(pathInfo)) {
            HttpSession session = req.getSession();
            session.invalidate();
        } else {
            System.out.println("do get login");
            UserDao userDao = new UserDao();
            HttpSession session = req.getSession();
            if (!session.isNew()) {
                String name = (String) session.getAttribute("name");
                String password = (String) session.getAttribute("password");
                User user = userDao.getUser(name, password);
                if (user != null) {
                    if (user.isAdmin()) {
                        resp.sendRedirect("/admin");
                    } else {
                        resp.sendRedirect("/user");
                    }
                } else {
                    req.getRequestDispatcher("/login.jsp").forward(req, resp);
                }
            } else {
                req.getRequestDispatcher("/login.jsp").forward(req, resp);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        log.info("doPost: " + req.getServletPath());
        UserDao userDao = new UserDao();
        String name = req.getParameter("name");
        String password = req.getParameter("password");
        User user = userDao.getUser(name, password);
        if (user != null) {
            HttpSession session = req.getSession();
            session.setAttribute("name", user.getName());
            session.setAttribute("password", user.getPassword());
            if (user.isAdmin()) {
                session.setAttribute("locale", Locale.ENGLISH.getLanguage());
                resp.sendRedirect("/admin");
            } else {
                resp.sendRedirect("/user");
            }
        } else {
            resp.sendError(403, "Wrong user");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("change locale");
        HttpSession session = req.getSession();
        String locale = (String) session.getAttribute("locale");
        locale = Locale.ENGLISH.getLanguage().equals(locale) ? "ru-Ru" : Locale.ENGLISH.getLanguage();
        session.setAttribute("locale", locale);
    }
}
