package servlet;

import dao.HotelDao;
import model.Hotel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.Map;

@WebServlet("/order")
public class OrderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HotelDao hotelDao = new HotelDao();
        String name = req.getParameter("name");
        int number = Integer.parseInt(req.getParameter("number"));
        String date = req.getParameter("date");
        if (hotelDao.insertHotel(new Hotel(name, number, date))) {
            req.setAttribute("successfully", "Your order is successfully booked.");
        } else {
            req.setAttribute("successfully", "Your is not booked");
        }
        req.getRequestDispatcher("/user.jsp").forward(req, resp);
    }

    private String quotate(String content) {
        return "'" + content + "'";
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
