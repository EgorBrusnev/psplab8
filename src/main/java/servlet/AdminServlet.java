package servlet;

import dao.HotelDao;
import model.Hotel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/admin/*")
public class AdminServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HotelDao hotelDao = new HotelDao();
        List<Hotel> hotels = hotelDao.getAllHotels();
        req.setAttribute("allInfo", hotels);
        req.getRequestDispatcher("/admin.jsp").forward(req, resp);
    }
}
