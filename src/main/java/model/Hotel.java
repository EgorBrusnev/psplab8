package model;

public class Hotel {

    private int number;
    private String name,date;

    public void setNumber(int number) {
        this.number = number;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public Hotel(String name, int number, String date) {
        this.name = name;
        this.number = number;
        this.date = date;
    }
}
