package model;

public class Room {
    private int id,free;
    private String type;

    public Room(int id, String type, int free) {
        this.id = id;
        this.free = free;
        this.type = type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFree(int free) {
        this.free = free;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public int getFree() {
        return free;
    }

    public String getType() {
        return type;
    }
}
