package model;

public class User {
    private int id;
    private String name;
    private String password;
    private int role;

    public User(String name, String password, int role) {
        this.role = role;
        this.name = name;
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return role == 1;
    }
}
