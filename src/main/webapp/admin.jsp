<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<fmt:setBundle basename="application"/>
<html>
<head>
    <title>Hostel</title>
</head>
<body>
<div style="text-align:center">
    <table class="result" style="text-align:center;">
        <caption><fmt:message key="admin.info"/></caption>
        <tr>
            <th><fmt:message key="admin.hotel.name"/></th>
            <th><fmt:message key="admin.hotel.number"/></th>
            <th><fmt:message key="admin.hotel.date"/></th>
        </tr>
        <%--@elvariable id="allInfo" type="java.util.List<Hotel>"--%>
        <c:forEach items="${allInfo}" var="number">
            <tr>
                <td>${number.name}</td>
                <td>${number.number}</td>
                <td>${number.date}</td>
            </tr>
        </c:forEach>
    </table>
    <button class="button"><fmt:message key="admin.order"/></button>
    <input type="button" id="logout" value="<fmt:message key="global.logout"/>"/>
    <input type="button" id="locale" value="<fmt:message key="global.change.locale"/>"/>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script type="text/javascript">
    <%@include file="js/login.js"%>
</script>
</body>
</html>
