<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
           prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<fmt:setBundle basename="application"/>
<html>
<head>
    <title>Hostel</title>
</head>
<body>
<div style="text-align: center">
    <form method="GET" action="order">
        <h4><fmt:message key="user.enter.name"/></h4>
        <input type="text" name="name">
        <h4><fmt:message key="user.enter.number"/></h4>
        <input type="number" name="number">
        <h4><fmt:message key="user.enter.date"/></h4>
        <input type="date" name="date">
        <h5>
            <c:out value="${successfully}"/><br>
        </h5>
        <input type="submit" value="<fmt:message key="user.order"/>"/>
        <input type="button" id="logout" value="<fmt:message key="global.logout"/>"/>
        <input type="button" id="locale" value="<fmt:message key="global.change.locale"/>"/>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script type="text/javascript">
    <%@include file="js/login.js"%>
</script>
</body>
</html>
