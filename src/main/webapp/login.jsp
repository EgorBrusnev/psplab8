<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
           prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<fmt:setBundle basename="application"/>
<html>
<head>
    <title>Hotel</title>
</head>
<body>
<div style="text-align: center" class="container">
    <form method="POST" action="/">
        <h4><fmt:message key="login.username"/></h4>
        <p>${pageContext.response.locale}</p>
        <input type="text" name="name"/>
        <br>
        <h4><fmt:message key="login.password"/></h4>
        <input type="password" name="password"/>
        <br>
        <c:out value="${validation}"/><br>
        <input type="submit" value="<fmt:message key="login.signin"/>" class="btn"/>
    </form>
    <input type="button" id="locale" value="<fmt:message key="global.change.locale"/>"/>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script type="text/javascript">
    <%@include file="js/login.js"%>
</script>
</body>
</html>
