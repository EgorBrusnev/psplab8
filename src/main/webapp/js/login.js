$(document).ready(() => {
    console.log("doc ready");
    $("#logout").click((e) => {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "/logout",
            success: () => {
                location.reload();
            }
        })
    });
    $("#locale").click((e) => {
        $.ajax({
            type: "PUT",
            url: "/locale",
            success: () => {
                location.reload();
            }
        })
    })
});